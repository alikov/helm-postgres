#!/usr/bin/env bash

# MIT license
# Copyright 2021 Sergej Alikov <sergej.alikov@gmail.com>

set -euo pipefail

declare -a REQUIRED_VARIABLES=(
    POSTGRES_PASSWORD_FILE
    POSTGRES_HOST
    POSTGRES_PORT
    POSTGRES_USERNAME
    KEEP_NR
    BACKUP_DIR
    ARCHIVE_DIR
)

declare var
for var in "${REQUIRED_VARIABLES[@]}"; do
    if [[ -z "${!var:+notempty}" ]]; then
        printf 'Required variable %s is not set\n' "$var"
        exit 1
    fi
done

PGPASSFILE=$(mktemp)
trap 'rm -f -- "$PGPASSFILE"' EXIT
chmod 0600 "$PGPASSFILE"

# https://www.postgresql.org/docs/12/libpq-pgpass.html
sed -e 's/\\/\\\\/g' -e 's/:/\\:/g' "$POSTGRES_PASSWORD_FILE" \
    | awk '{ print ENVIRON["POSTGRES_HOST"] ":*:replication:" ENVIRON["POSTGRES_USERNAME"] ":" $1; }' >"$PGPASSFILE"
export PGPASSFILE

declare label
label=$(date +%Y%m%d%H%M%S)

printf 'Backing up to %s\n' "${BACKUP_DIR%/}/${label}" >&2
pg_basebackup \
    --host="$POSTGRES_HOST" \
    --port="$POSTGRES_PORT" \
    --username="$POSTGRES_USERNAME" \
    --no-password \
    --pgdata="${BACKUP_DIR%/}/${label}" \
    --label="$label" \
    --format=tar \
    --gzip

find "${BACKUP_DIR%/}" ! -path "${BACKUP_DIR%/}" -type d \
    | sort \
    | head -n -"$KEEP_NR" \
    | grep -E '[0-9]{14}$' \
    | xargs -n 1 rm -rvf --


find "${ARCHIVE_DIR%/}" ! -path "${ARCHIVE_DIR%/}" -type f -name "*.backup" \
    | sort \
    | head -n -"$KEEP_NR" \
    | xargs -n 1 rm -vf --

declare oldest_backup
oldest_backup=$(set -e; cd "${ARCHIVE_DIR%/}"; find ./ -type f -name "*.backup" -mindepth 1 -maxdepth 1 | sed -e 's~^./~~' | sort | tail -n 1)

if [[ -n "$oldest_backup" ]]; then
    printf 'Removing WALs before %s\n' "$oldest_backup" >&2
    pg_archivecleanup "${ARCHIVE_DIR%/}" "$oldest_backup"
fi
