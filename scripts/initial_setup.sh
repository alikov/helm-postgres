#!/usr/bin/env bash

# MIT license
# Copyright 2021 Sergej Alikov <sergej.alikov@gmail.com>

set -euo pipefail


declare -a REQUIRED_VARIABLES=(
    POSTGRES_PASSWORD_FILE
    POSTGRES_HOST
    POSTGRES_PORT
    POSTGRES_USERNAME
    POSTGRES_DATABASE
    CONFIG_FILE
)

declare var
for var in "${REQUIRED_VARIABLES[@]}"; do
    if [[ -z "${!var:+notempty}" ]]; then
        printf 'Required variable %s is not set\n' "$var"
        exit 1
    fi
done

PGPASSFILE=$(mktemp)
trap 'rm -f -- "$PGPASSFILE"' EXIT
chmod 0600 "$PGPASSFILE"

# https://www.postgresql.org/docs/12/libpq-pgpass.html
sed -e 's/\\/\\\\/g' -e 's/:/\\:/g' "$POSTGRES_PASSWORD_FILE" \
    | awk '{ print ENVIRON["POSTGRES_HOST"] ":*:*:" ENVIRON["POSTGRES_USERNAME"] ":" $1; }' >"$PGPASSFILE"
export PGPASSFILE

until
    pg_isready \
        --host="$POSTGRES_HOST" \
        --port="$POSTGRES_PORT" \
        --username="$POSTGRES_USERNAME" \
        --dbname="$POSTGRES_DATABASE"
do
    printf 'Waiting until %s:%s is up\n' "$POSTGRES_HOST" "$POSTGRES_PORT" >&2
    sleep 1
done

# Config file is a Bash script with variable declarations.
# Below is an example content of a config file.
#
# declare -a PG_ROLE_NAMES=(u1 u2)
# declare -A PG_ROLES
# PG_ROLES[u1.password_file]=/postgres/secrets/u1
#
# declare -a PG_DATABASE_NAMES=(foo)
# declare -A PG_DATABASES
#
# PG_DATABASES[foo.owner]=u1
# optional:
# PG_DATABASES[foo.post_create_sql_file]=/postgres/post/foo.sql

# shellcheck disable=SC1090
source "$CONFIG_FILE"

declare role matching_roles password_file password
for role in "${PG_ROLE_NAMES[@]}"; do
    matching_roles=$(
        psql \
            --host="$POSTGRES_HOST" \
            --port="$POSTGRES_PORT" \
            --username="$POSTGRES_USERNAME" \
            --no-password \
            -v ON_ERROR_STOP=1 \
            -t -A -F '' \
            --set rolname="$role" \
            "$POSTGRES_DATABASE" <<"EOF"
SELECT COUNT(*) FROM pg_roles WHERE rolname=:'rolname'
EOF
                  )

    if ! [[ "$matching_roles" =~ ^[0-9]+$ ]]; then
        printf 'Unexpected result when looking up a number of existing roles matching %s (got %s)\n' \
               "$role" "$matching_roles" >&2
        exit 1
    fi

    if [[ "$matching_roles" -lt 1 ]]; then
        printf 'Creating role %s\n' "$role" >&2

        password_file="${PG_ROLES[${role}.password_file]}"

        # FIXME: It would be nice to not put the password
        # on the command line.
        password=$(cat "$password_file")

        psql \
            --host="$POSTGRES_HOST" \
            --port="$POSTGRES_PORT" \
            --username="$POSTGRES_USERNAME" \
            --no-password \
            -v ON_ERROR_STOP=1 \
            --set rolname="$role" \
            --set password="$password" \
            "$POSTGRES_DATABASE" <<"EOF"
CREATE ROLE :"rolname" WITH LOGIN PASSWORD :'password'
EOF
    fi
done

declare database matching_databases owner post_create_sql_file
for database in "${PG_DATABASE_NAMES[@]}"; do
    matching_databases=$(
        psql \
            --host="$POSTGRES_HOST" \
            --port="$POSTGRES_PORT" \
            --username="$POSTGRES_USERNAME" \
            --no-password \
            -v ON_ERROR_STOP=1 \
            -t -A -F '' \
            --set datname="$database" \
            "$POSTGRES_DATABASE" <<"EOF"
SELECT COUNT(*) FROM pg_database WHERE datname=:'datname'
EOF
                      )

    if ! [[ "$matching_databases" =~ ^[0-9]+$ ]]; then
        printf 'Unexpected result when looking up a number of existing databases matching %s (got %s)\n' \
               "$database" "$matching_databases" >&2
        exit 1
    fi

    if [[ "$matching_databases" -lt 1 ]]; then
        printf 'Creating database %s\n' "$database" >&2

        owner="${PG_DATABASES[${database}.owner]}"

        psql \
            --host="$POSTGRES_HOST" \
            --port="$POSTGRES_PORT" \
            --username="$POSTGRES_USERNAME" \
            --no-password \
            -v ON_ERROR_STOP=1 \
            --set database="$database" \
            --set owner="$owner" \
            "$POSTGRES_DATABASE" <<"EOF"
CREATE DATABASE :"database" OWNER :"owner"
EOF

        if [[ -n "${PG_DATABASES[${database}.post_create_sql_file]:+notempty}" ]]; then
            printf 'Executing the post-create SQL script for %s\n' "$database" >&2

            psql \
                --host="$POSTGRES_HOST" \
                --port="$POSTGRES_PORT" \
                --username="$POSTGRES_USERNAME" \
                --no-password \
                -v ON_ERROR_STOP=1 \
                "$database" < "${PG_DATABASES[${database}.post_create_sql_file]}"
        fi
    fi
done
